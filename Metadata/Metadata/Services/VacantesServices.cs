﻿using Metadata.DAL;
using Metadata.Models;
using Metadata.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Services
{
    public class VacantesServices
    {
        DAL_Vacante DAL_Vacante = new DAL_Vacante();
        DataManager DataManager = new DataManager();

        /// <summary>
        /// Método que recupera las vacantes
        /// si contiene el parametro id valor obtiene la vacante relacionada con el id, 
        /// pero si no contiene valor recupera todas las vacantes disponibles
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response GetVacantes(int? id)
        {
            return DAL_Vacante.GetVacantes(id);
        }

        /// <summary>
        /// Método que recupera los conocimientos requeridos para la vacante
        /// </summary>
        /// <param name="idVacante"></param>
        /// <returns></returns>
        public Response GetSkillsRequired(int id = 0)
        {
            return DAL_Vacante.GetSkillsRequired(id);
        }

        /// <summary>
        /// Método que recupera los conocimientos deseados para la vacante
        /// </summary>
        /// <param name="idVacante"></param>
        /// <returns></returns>
        public Response obtieneConocimientosDeseados(int id)
        {
            return DAL_Vacante.obtieneConocimientosDeseados(id);
        }

        /// <summary>
        /// Método que recupera los idiomas para la vacante
        /// </summary>
        /// <param name="vacancyId"></param>
        /// <returns></returns>
        public Response GetLenguaches(int id)
        {
            return DAL_Vacante.GetLenguaches(id);
        }

        /// <summary>
        /// Método que recupera las vacantes relacionadas para la vacante
        /// </summary>
        /// <param name="vacancyId"></param>
        /// <returns></returns>
        public Response GetRelatedVacancies(int id)
        {
            var vacantes = DAL_Vacante.GetRelatedVacancies(id);
            if (vacantes.IsSuccess)
            {
                foreach(var v in (List<Vacante>)vacantes.Result)
                {
                    v.url = $"{DataManager.GetURLRedirect()}{v.idVacante}";
                }
            }
            return vacantes;
        }
    }
}