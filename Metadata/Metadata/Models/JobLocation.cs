﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleJobsApi_Test
{
    public class JobLocation
    {
        [JsonProperty("@type")]
        public string type { get; set; }
        public Address address { get; set; }

        public JobLocation()
        {
            address = new Address();
        }
    }
}
