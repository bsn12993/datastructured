﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Models
{
    public class Vacante
    {
        public List<Conocimientos> Conocimientos { get; set; }
        public List<ConocimientosDeseados> ConocimientosDeseados { get; set; }
        public List<Idioma> Idiomas { get; set; }
        public List<Vacante> ListaVacantesRelacionadas { get; set; }
        public string url { get; set; }
        public Int32 idVacante { get; set; }

        /// <summary>
        /// nombrePuesto
        /// </summary>
        public String nombrePuesto { get; set; }

        /// <summary>
        /// aniosExperiencia
        /// </summary>
        public String aniosExperiencia { get; set; }

        /// <summary>
        /// certificaciones
        /// </summary>
        public String certificaciones { get; set; }

        /// <summary>
        /// edadMinima
        /// </summary>
        public Int32 edadMinima { get; set; }
        public Int32 edadMaxima { get; set; }

        public String observaciones { get; set; }

        public Decimal sueldoBruto { get; set; }
        public Decimal sueldoFin { get; set; }
        public Int32 viajar { get; set; }
        public String apoyoViaticos { get; set; }
        //public String responsableCta { get; set; }
        public String fechaInicioProyecto { get; set; }
        public String duracionProyecto { get; set; }
        public String fechaVigencia { get; set; }
        public String horario { get; set; }
        public Int32 numeroPostular { get; set; }
        public Int32 numeroCubrir { get; set; }
        public String posibilidadExtenderContrato { get; set; }
        public String duracionContrato { get; set; }
        public String precioVenta { get; set; }
        public String recomendacionAdicional { get; set; }
        public String direccion { get; set; }
        public Int32 idResponsableCta { get; set; }
        public Int32 idSexo { get; set; }
        public Int32 IdNivelEstudio { get; set; }
        public Int32 idEstado { get; set; }
        public Int32 idMunicipio { get; set; }
        public Int32 idTipoContratacion { get; set; }
        public Int32 idCategoria { get; set; }
        public Int32 idCompania { get; set; }
        public Int32 idCliente { get; set; }
        public Boolean bMostrarDireccion { get; set; }
        public Boolean bMostrarSexo { get; set; }
        public Boolean bMostrarEdad { get; set; }
        public Boolean bMostrarObservaciones { get; set; }
        public Boolean bMostrarSueldo { get; set; }
        public Boolean bMostrarViajar { get; set; }
        public Boolean bMostrarCategoria { get; set; }
        public Boolean bMostrarDirPuesto { get; set; }
        public Boolean bMostrarCliente { get; set; }
        public Int32 idEstatus { get; set; }
        public Int32 idRespTecnico1 { get; set; }
        public Int32 idRespTecnico2 { get; set; }
        public Int32 idResponsableRH { get; set; }
        public Int32 nIdSecuencia { get; set; }
        public string numeroCubrirCadena { get; internal set; }
        public string numeroPostularCadena { get; internal set; }
        public object genero { get; internal set; }
        public string nivelAcademico { get; internal set; }
        public string estadoCadena { get; internal set; }
        public string municipioCadena { get; internal set; }
        public string tipoContratacion { get; internal set; }
        public string Categoria { get; internal set; }
        public string Compania { get; internal set; }
        public string cliente { get; internal set; }
        public string fechaCreacion { get; internal set; }
        public string viajarCadena { get; internal set; }
    }
}