﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Models
{
    public class Conocimientos
    {
        public int nIdConocimiento { get; internal set; }
        public string sDescripcion { get; internal set; }
        public string sAniosExperienciaCadena { get; internal set; }
        public string sNivelExperiencia { get; internal set; }
    }
}