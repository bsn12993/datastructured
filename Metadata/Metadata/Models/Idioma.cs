﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Models
{
    public class Idioma
    {
        public int nIdIdioma { get; internal set; }
        public object sDescripcion { get; internal set; }
        public int nIdVacante { get; internal set; }
        public object Nivel { get; internal set; }
    }
}