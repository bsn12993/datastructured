﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleJobsApi_Test
{
    public class JobPosting
    {
        [JsonProperty("@context")]
        public string context { get; set; }
        [JsonProperty("@type")]
        public string type { get; set; }
        public string datePosted { get; set; }
        public string description { get; set; }
        public string title { get; set; } = "";
        public string employmentType { get; set; }
        public HiringOrganization hiringOrganization { get; set; }
        public JobLocation jobLocation { get; set; }
        public Identifier identifier { get; set; }
        public BaseSalary baseSalary { get; set; }
        public string validThrough { get; set; }

        public JobPosting()
        {
            hiringOrganization = new HiringOrganization();
            jobLocation = new JobLocation();
            identifier = new Identifier();
            baseSalary = new BaseSalary();
            datePosted = string.Empty;
            description = string.Empty;
            title = string.Empty;
            employmentType = string.Empty;
            validThrough = string.Empty;

        }
    }
}
