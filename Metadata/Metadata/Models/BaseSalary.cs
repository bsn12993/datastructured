﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleJobsApi_Test
{
    public class BaseSalary
    {
        [JsonProperty("@type")]
        public string type { get; set; }
        public string currency { get; set; }
        public Value value { get; set; }

        public BaseSalary()
        {
            type = string.Empty;
            currency = string.Empty;
            value = new Value();
        }
    }
}
