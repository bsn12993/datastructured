﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Models
{
    public class ConocimientosDeseados
    {
        public int nIdConocimientoDeseado { get; internal set; }
        public object sDescripcion { get; internal set; }
        public int nIdVacante { get; internal set; }
        public int nAniosExperiencia { get; internal set; }
        public object sNivelExperiencia { get; internal set; }
    }
}