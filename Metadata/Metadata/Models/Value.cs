﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleJobsApi_Test
{
    public class Value
    {
        [JsonProperty("@type")]
        public string type { get; set; }
        public decimal value { get; set; } 
        public string unitText { get; set; }

        public Value()
        {
            type = string.Empty;
            value = 0;
            unitText = string.Empty;
        }
    }
}
