﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleJobsApi_Test
{
    public class Address
    {
        [JsonProperty("@type")]
        public string type { get; set; }
        public string addressLocality { get; set; }
        public string addressRegion { get; set; }
        public string addressCountry { get; set; }

        public Address()
        {
            type = string.Empty;
            addressCountry= string.Empty; ;
            addressLocality = string.Empty;
            addressRegion = string.Empty;
        }
    }
}
