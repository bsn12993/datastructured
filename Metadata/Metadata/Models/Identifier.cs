﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleJobsApi_Test
{
    public class Identifier
    {
        [JsonProperty("@type")]
        public string type { get; set; }
        public string name { get; set; }
        public string value { get; set; }

        public Identifier()
        {
            type = string.Empty;
            name = string.Empty;
            value = string.Empty;
        }
    }
}
