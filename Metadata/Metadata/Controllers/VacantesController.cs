﻿using GoogleJobsApi_Test;
using Metadata.Models;
using Metadata.Services;
using Metadata.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Metadata.Controllers
{
    public class VacantesController : Controller
    {
        VacantesServices VacantesServices = new VacantesServices();
        DataManager DataManager = new DataManager();

        // GET: Vacantes
        public ActionResult Index()
        {
            int? id = 0;
            var vacantes = VacantesServices.GetVacantes(id);//se consultan las vacantes
            if (vacantes.IsSuccess)
            {
                var json = JsonHelper.setDataListJson((List<Vacante>)vacantes.Result);//se convierte en json el resultado
                ViewBag.JSONLD = json;//se asigna a un viewbag para enviarlo a la vista
                return View((List<Vacante>)vacantes.Result);//se envia el modelo a la vista
            }
            else return View(new List<Vacante>());
        }


        public ActionResult Details(int? id)
        {
            var vacante = VacantesServices.GetVacantes(id);//se consulta la vacante
            if (vacante.IsSuccess)
            {
                var v = ((List<Vacante>)vacante.Result).SingleOrDefault();//se recupera la unica vacante de la lista
                v.url = $"{DataManager.GetURLRedirect()}?vid={v.idVacante}";

                var conocimientos = VacantesServices.GetSkillsRequired(v.idVacante);//se recupera los conocimientos relacionados a la vacante
                if (conocimientos.IsSuccess) v.Conocimientos = (List<Conocimientos>)conocimientos.Result;

                var conocimientosDeseados = VacantesServices.obtieneConocimientosDeseados(v.idVacante);//se recupera los conocimientos relacionados a la vacante
                if (conocimientosDeseados.IsSuccess) v.ConocimientosDeseados = (List<ConocimientosDeseados>)conocimientosDeseados.Result;

                var idiomas = VacantesServices.GetLenguaches(v.idVacante);//se recupera los idiomas relacionados a la vacante
                if (idiomas.IsSuccess) v.Idiomas = (List<Idioma>)idiomas.Result;

                var vacantesRelacionadas = VacantesServices.GetRelatedVacancies(v.idVacante);//se recupera las vacantes relacionadas a la vacante
                if (vacantesRelacionadas.IsSuccess) v.ListaVacantesRelacionadas = (List<Vacante>)vacantesRelacionadas.Result;

                var json = JsonHelper.setDataJson(v);//se convierte en json el objeto
                ViewBag.JSONLD = json;//el json se envia a la vista por un viewbag
                return View(v);//se envia el modelo a la vista
            }
            else return View(new List<Vacante>());
        }

         


 
    }
}