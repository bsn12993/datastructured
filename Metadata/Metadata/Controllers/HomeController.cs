﻿using GoogleJobsApi_Test;
using Metadata.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Metadata.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult AboutSearch(int id = 0)
        {
            string a = "";
            if (id == 0) return RedirectToAction("About");
            if (id == 1)
                a = GetDate();
            else if (id == 2)
                a = GetDate2();
            ViewBag.JSON = a;
            string hola = "HOLA";
            return View(new Article() { Body = hola });
        }

        public class Test
        {
            public string Message { get; set; }
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public string ConvertirFormatoFecha(string val)
        {
            string date = string.Empty;
            try
            {
                var v = val.Split(' ');
                var d = v[0];
                var m = v[1];
                var y = v[2];

                if (m.ToLower().Equals("enero")) m = "1";
                if (m.ToLower().Equals("febrero")) m = "2";
                if (m.ToLower().Equals("marzo")) m = "3";
                if (m.ToLower().Equals("abril")) m = "4";
                if (m.ToLower().Equals("mayo")) m = "5";
                if (m.ToLower().Equals("junio")) m = "6";
                if (m.ToLower().Equals("julio")) m = "7";
                if (m.ToLower().Equals("agosto")) m = "8";
                if (m.ToLower().Equals("septiembre")) m = "9";
                if (m.ToLower().Equals("octubre")) m = "10";
                if (m.ToLower().Equals("noviembre")) m = "11";
                if (m.ToLower().Equals("diciembre")) m = "12";
                return string.Format("{0}-{1}-{2}", y, m, d);
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }

        public string GetDate()
        {
            JobPosting jobPosting = new JobPosting();
            jobPosting.context = "https://schema.org/";
            jobPosting.type = "JobPosting";
            jobPosting.title = "Titulo 1";
            jobPosting.description = "Descripcion 1";
            jobPosting.datePosted = "2019-10-08";
            jobPosting.jobLocation = new JobLocation()
            {
                type = "Place",
                address = new Address()
                {
                    type = "PostalAddress",
                    addressCountry = "MËXICO",
                    addressLocality = "MËXICO"
                }
            };
            jobPosting.hiringOrganization = new HiringOrganization()
            {
                type = "Organization",
                name = "TasiSoluciones"
            };

            var json = JsonConvert.SerializeObject(jobPosting);
            return json;
        }


        public string GetDate2()
        {
            JobPosting jobPosting = new JobPosting();
            jobPosting.context = "https://schema.org/";
            jobPosting.type = "JobPosting";
            jobPosting.title = "Titulo 2";
            jobPosting.description = "DESCRIPCION PRUEBA 2";
            jobPosting.datePosted = "2019-10-12";
            jobPosting.jobLocation = new JobLocation()
            {
                type = "Place",
                address = new Address()
                {
                    type = "PostalAddress",
                    addressCountry = "MËXICO",
                    addressLocality = "MËXIC0"
                }
            };
            jobPosting.hiringOrganization = new HiringOrganization()
            {
                type = "Organization",
                name = "TasiSoluciones"
            };

            var json = JsonConvert.SerializeObject(jobPosting);
            return json;
        }

    }
}