﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Metadata.ConexionBD
{
    public class DbManager
    {
        public string Ambiente { get; set; }
        public string cnsProd { get; set; }
        public string cnsQa { get; set; }
        public string cnsDev { get; set; }

        public DbManager()
        {
            this.Ambiente = ConfigurationManager.AppSettings["strAmbiente"].ToString();
            this.cnsProd = ConfigurationManager.ConnectionStrings["cnsDev"].ConnectionString;
            this.cnsQa = ConfigurationManager.ConnectionStrings["cnsQa"].ConnectionString;
            this.cnsDev = ConfigurationManager.ConnectionStrings["cnsDev"].ConnectionString;
        }

       /// <summary>
       /// Se recupera la cadena de conexión
       /// </summary>
       /// <returns></returns>
        public string GetConnectiongString()
        {
            string connectionString = string.Empty;
            switch (this.Ambiente)
            {
                case "PROD":connectionString = this.cnsProd;break;
                case "QA":connectionString = this.cnsQa;break;
                case "DEV":connectionString = this.cnsDev;break;
            }
            return connectionString;
        }



    }
}