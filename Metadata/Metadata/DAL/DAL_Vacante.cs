﻿using Metadata.ConexionBD;
using Metadata.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Metadata.DAL
{
    public class DAL_Vacante
    {
        private string connectionString = string.Empty;
        DbManager DbManager = null;
        SqlParameter[] parameters = null;

        /// <summary>
        /// Método que recupera las vacantes
        /// si contiene el parametro id valor obtiene la vacante relacionada con el id, 
        /// pero si no contiene valor recupera todas las vacantes disponibles
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response GetVacantes(int? id)
        {
            DbManager = new DbManager();
            connectionString = DbManager.GetConnectiongString();
            List<Vacante> vacantes = new List<Vacante>();
            parameters = new SqlParameter[]
            {

                new SqlParameter("@companyId",null),
                new SqlParameter("@vacancyId", (id!=0)?id:null),
                new SqlParameter("@state", null),
                new SqlParameter("@city", null),
                new SqlParameter("@category", null),
                new SqlParameter("@searchPattern", null)
            };
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("SEL_Vacancies_SP", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Vacante vacancy = new Vacante()
                                {
                                    idVacante = int.Parse(reader["vacancyId"].ToString()),
                                    nombrePuesto = reader["position"].ToString(),
                                    aniosExperiencia = reader["experience"].ToString(),
                                    certificaciones = reader["certifications"].ToString(),
                                    edadMinima = reader["minimumAge"].ToString() == "No Especificada" ? 0 : int.Parse(reader["minimumAge"].ToString()),
                                    edadMaxima = reader["maximumAge"].ToString() == "No Especificada" ? 0 : int.Parse(reader["maximumAge"].ToString()),
                                    observaciones = reader["sDescription"].ToString(),
                                    sueldoBruto = reader["salary"].ToString() == "No Especificado" ? 0 : decimal.Parse(reader["salary"].ToString()),
                                    viajarCadena = reader["travel"].ToString(),
                                    fechaCreacion = reader["stringCreationDate"].ToString(),
                                    //fechaInicioProyecto = drVacancy["stringStartDate"].ToString(),
                                    //duracionProyecto = drVacancy["duration"].ToString(),
                                    horario = reader["workingHours"].ToString(),
                                    numeroCubrirCadena = reader["numberVacancies"].ToString(),
                                    numeroPostularCadena = reader["numberPostulates"].ToString(),
                                    genero = reader["sex"].ToString(),
                                    nivelAcademico = reader["academicLevel"].ToString(),
                                    estadoCadena = reader["state"].ToString(),
                                    municipioCadena = reader["city"].ToString(),
                                    tipoContratacion = reader["city"].ToString(),
                                    Categoria = reader["category"].ToString(),
                                    Compania = reader["company"].ToString(),
                                    cliente = reader["customer"].ToString(),
                                    bMostrarCategoria = (bool)reader["bMostrarCategoria"],
                                    bMostrarEdad = (bool)reader["bMostrarEdad"],
                                    bMostrarObservaciones = (bool)reader["bMostrarObservaciones"],
                                    bMostrarDirPuesto = (bool)reader["bMostrarDirPuesto"],
                                    bMostrarSexo = (bool)reader["bMostrarSexo"],
                                    bMostrarSueldo = (bool)reader["bMostrarSueldo"],
                                    bMostrarViajar = (bool)reader["bMostrarViajar"],
                                    bMostrarCliente = (bool)reader["bMostrarCliente"],
                                    //ListConocimientosRequeridos = this.GetSkillsRequired(int.Parse(drVacancy["vacancyId"].ToString())),
                                    //ListConocimientosDeseados = this.GetSkillsDesired(int.Parse(drVacancy["vacancyId"].ToString())),
                                };

                                vacantes.Add(vacancy);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Result = vacantes,
                                Message = "Se encontrarón vacantes"
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Result = vacantes,
                                Message = "No se encontrarón vacantes"
                            };
                        }
                    }
                }
            }
            catch(Exception e)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = e.Message,
                    Result = null
                };
            }
        }

        /// <summary>
        /// Método que recupera los conocimientos requeridos para la vacante
        /// </summary>
        /// <param name="idVacante"></param>
        /// <returns></returns>
        public Response GetSkillsRequired(int idVacante)
        {
            List<Conocimientos> conocimientos = new List<Conocimientos>();
            parameters = new SqlParameter[]
            {
                new SqlParameter("@vacanciId",idVacante)
            };
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("Sel_GetSkillsRequired_SP", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Conocimientos skill = new Conocimientos()
                                {

                                    nIdConocimiento = int.Parse(reader["skillId"].ToString()),
                                    sDescripcion = reader["description"].ToString(),
                                    sAniosExperienciaCadena = reader["yearExperience"].ToString(),
                                    sNivelExperiencia = reader["level"].ToString()
                                };

                                conocimientos.Add(skill);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Result = conocimientos,
                                Message = "Se encontrarón concimientos"
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Result = conocimientos,
                                Message = "No se encontrarón vacantes"
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = null
                };
            }
        }

        /// <summary>
        /// Método que recupera los conocimientos deseados para la vacante
        /// </summary>
        /// <param name="idVacante"></param>
        /// <returns></returns>
        public Response obtieneConocimientosDeseados(int idVacante)
        {
            List<ConocimientosDeseados> ListConocimientosDeseados = new List<ConocimientosDeseados>();
            parameters = new SqlParameter[]
            {
                new SqlParameter("@vacanciId",idVacante)
            };
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("Sel_GetSkillsDesired_SP", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                ConocimientosDeseados conoDeseados = new ConocimientosDeseados();
                                conoDeseados.nIdConocimientoDeseado = Int32.Parse(reader["nIdConocimientoDeseado"].ToString());
                                conoDeseados.sDescripcion = reader["sDescripcion"].ToString();
                                conoDeseados.nIdVacante = int.Parse(reader["nIdVacante"].ToString());
                                if (reader["nAniosExperiencia"] != DBNull.Value)
                                {
                                    if (reader["nAniosExperiencia"].ToString().Trim() != string.Empty)
                                        conoDeseados.nAniosExperiencia = int.Parse(reader["nAniosExperiencia"].ToString());
                                }
                                conoDeseados.sNivelExperiencia = reader["sNivelExperiencia"].ToString();
                                ListConocimientosDeseados.Add(conoDeseados);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Result = ListConocimientosDeseados,
                                Message = "Se encontrarón concimientos"
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Result = ListConocimientosDeseados,
                                Message = "No se encontrarón vacantes"
                            };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = null
                };
            }
        }

        /// <summary>
        /// Método que recupera los idiomas para la vacante
        /// </summary>
        /// <param name="vacancyId"></param>
        /// <returns></returns>
        public Response GetLenguaches(int vacancyId)
        {
            List<Idioma> idiomas = new List<Idioma>();
            parameters = new SqlParameter[]
            {
                new SqlParameter("@vacancyId",vacancyId)
            };
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("SEL_LenguachesByVacancy_SP", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Idioma skill = new Idioma()
                                {
                                    nIdIdioma = int.Parse(reader["nIdIdioma"].ToString()),
                                    sDescripcion = reader["sDescripcion"].ToString(),
                                    nIdVacante = int.Parse(reader["nIdVacante"].ToString()),
                                    Nivel = reader["Nivel"].ToString()

                                };

                                idiomas.Add(skill);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Result = idiomas,
                                Message = "Se encontrarón concimientos"
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Result = idiomas,
                                Message = "No se encontrarón vacantes"
                            };
                        }
                    }
                }

                
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = null
                };
            }
        }

        /// <summary>
        /// Método que recupera las vacantes relacionadas para la vacante
        /// </summary>
        /// <param name="vacancyId"></param>
        /// <returns></returns>
        public Response GetRelatedVacancies(int vacancyId)
        {
            parameters = new SqlParameter[]
            {
                new SqlParameter("@vacancyId",vacancyId)
            };
            List<Vacante> vacantes = new List<Vacante>();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    using (var cmd = new SqlCommand("SEL_GetRelatedVacancies_SP", connection))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        connection.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                Vacante vacancy = new Vacante()
                                {
                                    idVacante = int.Parse(reader["vacancyId"].ToString()),
                                    nombrePuesto = reader["position"].ToString(),
                                    cliente = reader["customer"].ToString(),
                                    bMostrarCliente = (bool)reader["bMostrarCliente"],
                                };

                                vacantes.Add(vacancy);
                            }
                            return new Response
                            {
                                IsSuccess = true,
                                Result = vacantes,
                                Message = "Se encontrarón concimientos"
                            };
                        }
                        else
                        {
                            return new Response
                            {
                                IsSuccess = false,
                                Result = vacantes,
                                Message = "No se encontrarón vacantes"
                            };
                        }
                    }
                }         
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = null
                };
            }

        }
    }
}