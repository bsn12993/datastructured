﻿using GoogleJobsApi_Test;
using Metadata.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Util
{
    public static class JsonHelper
    {
        /// <summary>
        /// Método que convierte el modelo a json
        /// </summary>
        /// <param name="vacante"></param>
        /// <returns></returns>
        public static string setDataJson(Vacante vacante)
        {
            JobPosting jobPosting = new JobPosting();
            jobPosting.context = "https://schema.org/";
            jobPosting.type = "JobPosting";
            jobPosting.title = vacante.nombrePuesto;
            jobPosting.description = vacante.observaciones;
            jobPosting.datePosted = Util.ConvertirFormatoFecha(vacante.fechaCreacion);
            jobPosting.validThrough = Util.ConvertirFormatoFecha(vacante.fechaVigencia);
            jobPosting.jobLocation = new JobLocation()
            {
                type = "Place",
                address = new Address()
                {
                    type = "PostalAddress",
                    addressCountry = vacante.estadoCadena,
                    addressLocality = vacante.municipioCadena
                }
            };
            jobPosting.hiringOrganization = new HiringOrganization()
            {
                type = "Organization",
                name = "TasiSoluciones"
            };
            jobPosting.identifier = new Identifier
            {
                type = "PropertyValue"
            };
            jobPosting.baseSalary = new BaseSalary
            {
                type = "MonetaryAmount",
                currency = "MXN",
                value = new Value
                {
                    type = "QuantitativeValue",
                    value = vacante.sueldoBruto,
                    unitText = "MONTH"
                }
            };
            var json = JsonConvert.SerializeObject(jobPosting);
            return json;
        }

        /// <summary>
        /// Método que convierte el modelo a json
        /// </summary>
        /// <param name="vacantes"></param>
        /// <returns></returns>
        public static string setDataListJson(List<Vacante> vacantes)
        {
            List<JobPosting> jobPostings = new List<JobPosting>();
            JobPosting jobPosting = null;
            foreach (var i in vacantes)
            {
                jobPosting = new JobPosting();
                jobPosting.context = "https://schema.org/";
                jobPosting.type = "JobPosting";
                jobPosting.title = i.nombrePuesto;
                jobPosting.description = i.observaciones;
                jobPosting.datePosted = Util.ConvertirFormatoFecha(i.fechaCreacion);
                jobPosting.validThrough = Util.ConvertirFormatoFecha(i.fechaVigencia);
                jobPosting.jobLocation = new JobLocation()
                {
                    type = "Place",
                    address = new Address()
                    {
                        type = "PostalAddress",
                        addressCountry = i.estadoCadena,
                        addressLocality = i.municipioCadena
                    }
                };
                jobPosting.hiringOrganization = new HiringOrganization()
                {
                    type = "Organization",
                    name = "TasiSoluciones"
                };
                jobPosting.identifier = new Identifier
                {
                    type = "PropertyValue"
                };
                jobPosting.baseSalary = new BaseSalary
                {
                    type = "MonetaryAmount",
                    currency = "MXN",
                    value = new Value
                    {
                        type = "QuantitativeValue",
                        value = i.sueldoBruto,
                        unitText = "MONTH"
                    }
                };
                jobPostings.Add(jobPosting);
            }

            var json = JsonConvert.SerializeObject(jobPostings);
            return json;
        }
    }
}