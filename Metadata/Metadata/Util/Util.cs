﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Metadata.Util
{
    public static class Util
    {
        public static string ConvertirFormatoFecha(string val)
        {
            string date = string.Empty;
            try
            {
                var v = val.Split(' ');
                var d = v[0];
                var m = v[1];
                var y = v[2];

                if (m.ToLower().Equals("enero")) m = "1";
                if (m.ToLower().Equals("febrero")) m = "2";
                if (m.ToLower().Equals("marzo")) m = "3";
                if (m.ToLower().Equals("abril")) m = "4";
                if (m.ToLower().Equals("mayo")) m = "5";
                if (m.ToLower().Equals("junio")) m = "6";
                if (m.ToLower().Equals("julio")) m = "7";
                if (m.ToLower().Equals("agosto")) m = "8";
                if (m.ToLower().Equals("septiembre")) m = "9";
                if (m.ToLower().Equals("octubre")) m = "10";
                if (m.ToLower().Equals("noviembre")) m = "11";
                if (m.ToLower().Equals("diciembre")) m = "12";
                return string.Format("{0}-{1}-{2}", y, m, d);
            }
            catch (Exception e)
            {
                return string.Empty;
            }
        }
    }
}