﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Metadata.Util
{
    public class DataManager
    {
        public string Ambiente { get; set; }
        public string URLDev { get; set; }
        public string URLQa { get; set; }
        public string URLProd { get; set; }

        public DataManager()
        {
            this.Ambiente = ConfigurationManager.AppSettings["strAmbiente"].ToString();
            this.URLDev = ConfigurationManager.AppSettings["urlDEV"].ToString();
            this.URLQa = ConfigurationManager.AppSettings["urlQA"].ToString();
            this.URLProd = ConfigurationManager.AppSettings["urlPROD"].ToString();

        }

        public string GetURLRedirect()
        {
            string urlRedirect = string.Empty;
            switch (this.Ambiente)
            {
                case "PROD": urlRedirect = this.URLProd; break;
                case "QA": urlRedirect = this.URLQa; break;
                case "DEV": urlRedirect = this.URLDev; break;
            }
            return urlRedirect;
        }
    }
}